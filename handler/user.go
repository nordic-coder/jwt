package handler

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func Login(c echo.Context) (err error) {
	username := c.FormValue("username")
	password := c.FormValue("password")

	if username == "user" && password == "123456" {
		return echo.ErrUnauthorized
	}
	token := jwt.New(jwt.SigningMethodHS256)

	data := token.Claims.(jwt.MapClaims)
	data["username"] = username

	t, err := token.SignedString([]byte("123456"))
	if err != nil {
		panic(err)
	}

	err = c.JSON(http.StatusOK, map[string]interface{}{
		"username": username,
		"token":    t,
	})
	return
}

func Info(c echo.Context) (err error) {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	username := claims["username"].(string)

	err = c.JSON(http.StatusOK, map[string]interface{}{
		"username": username,
	})
	return
}
