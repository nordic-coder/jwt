package main

import (
	"net/http"

	"gitlab.com/nordic-coder/jwt/handler"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func hello(c echo.Context) (err error) {
	err = c.String(http.StatusOK, "Hello, World!")
	return
}

func main() {
	e := echo.New()
	e.POST("/login", handler.Login)

	g := e.Group("/user", middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte("123456"),
	}))

	g.POST("/info", handler.Info)

	e.Logger.Fatal(e.Start(":1323"))
}
